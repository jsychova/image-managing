import { Directive, Input, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
    selector: '[tooltip]'
})
export class TooltipDirective {
    @Input('tooltip') tooltipTitle: string;
    @Input() placement: string;
    tooltip: HTMLElement;
    offset = 10;

    constructor(private el: ElementRef, private renderer: Renderer2) { }

    @HostListener('mouseenter') onMouseEnter() {
        if (!this.tooltip && this.tooltipTitle) { this.show(); }
    }

    @HostListener('mouseleave') onMouseLeave() {
        if (this.tooltip) { this.hide(); }
    }

    show() {
        this.create();
        this.setPosition();
        this.renderer.addClass(this.tooltip, 'tooltip--show');
    }

    hide() {
        this.renderer.removeClass(this.tooltip, 'tooltip-show');
        this.renderer.removeChild(document.body, this.tooltip);
        this.tooltip = null;
    }

    create() {
        this.tooltip = this.renderer.createElement('span');

        this.renderer.appendChild(
            this.tooltip,
            this.renderer.createText(this.tooltipTitle)
        );

        this.renderer.appendChild(document.body, this.tooltip);
        this.renderer.addClass(this.tooltip, 'tooltip');
        this.renderer.addClass(this.tooltip, `tooltip--${this.placement}`);
    }

    setPosition() {
        const hostPosition = this.el.nativeElement.getBoundingClientRect();
        const tooltipPosition = this.tooltip.getBoundingClientRect();
        const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

        let top, left;

        if (this.placement === 'top') {
            top = hostPosition.top - tooltipPosition.height - this.offset;
            left = hostPosition.left + (hostPosition.width - tooltipPosition.width) / 2;
        }

        if (this.placement === 'bottom') {
            top = hostPosition.bottom + this.offset;
            left = hostPosition.left + (hostPosition.width - tooltipPosition.width) / 2;
        }

        if (this.placement === 'left') {
            top = hostPosition.top + (hostPosition.height - tooltipPosition.height) / 2;
            left = hostPosition.left - tooltipPosition.width - this.offset;
        }

        if (this.placement === 'right') {
            top = hostPosition.top + (hostPosition.height - tooltipPosition.height) / 2;
            left = hostPosition.right + this.offset;
        }

        this.renderer.setStyle(this.tooltip, 'top', `${top + scrollPosition}px`);
        this.renderer.setStyle(this.tooltip, 'left', `${left}px`);
    }
}

