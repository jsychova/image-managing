import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';

import { AppComponent } from './app.component';
import { AppRoutingModule, routedComponents } from './app-routing.module';
import { ImagesService } from './services/images.service';
import { TooltipDirective } from './directives/tooltip.directive';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        AppComponent,
        routedComponents,
        TooltipDirective,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
    ],
    providers: [
        ImagesService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
