import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImagesManagingComponent } from './components/images-managing/images-managing.component';
import { ImagesAddComponent } from './components/images-add/images-add.component';

const routes: Routes = [
    {path: '', component: ImagesManagingComponent},
    {path: 'create', component: ImagesAddComponent},
    {path: 'edit/:id', component: ImagesAddComponent},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule,
    ],
})

export class AppRoutingModule {}

export const routedComponents = [
    ImagesManagingComponent,
    ImagesAddComponent,
];
