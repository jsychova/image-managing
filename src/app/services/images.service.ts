import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class ImagesService {

    create(form) {

    }

    getOne(id: number): Image {
        return images.find(image => image.id === id);
    }

    getAll(): Image[] {
        return images;
    }

    delete(id: number) {
        images = images.filter((image) => image.id !== id);
    }
}


export interface Image {
    id?: number;
    img: string;
    description: string;
}


let images: Image[] = [
    {
        id: 1,
        img: 'https://picsum.photos/200',
        description: 'Description 1'
    },
    {
        id: 2,
        img: 'https://picsum.photos/200/300/?random',
        description: 'Description 2'
    },
    {
        id: 3,
        img: 'https://picsum.photos/g/200/300',
        description: 'Description 3'
    },
    {
        id: 4,
        img: 'https://picsum.photos/200/300?image=0',
        description: 'Description 4'
    },
    {
        id: 5,
        img: 'https://picsum.photos/200/300?image=0',
        description: 'Description 5'
    }
];
