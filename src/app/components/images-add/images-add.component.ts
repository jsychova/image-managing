import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Image, ImagesService } from '../../services/images.service';

@Component({
    selector: 'app-images-add',
    templateUrl: './images-add.component.html',
    styleUrls: ['./images-add.component.scss']
})
export class ImagesAddComponent implements OnInit {
    selectedFile: File;
    form: Image = {
        description: '',
        img: ''
    };

    private currentImageId: string;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private imagesService: ImagesService,
    ) {
    }

    ngOnInit() {
        this.currentImageId = this.activatedRoute.snapshot.paramMap.get('id');

        if (this.currentImageId) {
            this.form = this.imagesService.getOne(parseInt(this.currentImageId));
        }
    }

    fileEvent(event) {
        if (!event.target.files || !event.target.files.length) {
            return;
        }
        this.form.img = URL.createObjectURL(event.target.files[0]);
        this.selectedFile = event.target.files[0];
    }

    createImage() {
        const uploadData = new FormData();
        uploadData.append('img', this.selectedFile);
        uploadData.append('description', this.form.description);

        this.imagesService.create(uploadData);
        this.router.navigateByUrl('/');
    }
}
