import { Component, OnInit } from '@angular/core';

import { Image, ImagesService } from '../../services/images.service';

@Component({
    selector: 'app-images-managing',
    templateUrl: './images-managing.component.html',
    styleUrls: ['./images-managing.component.scss']
})

export class ImagesManagingComponent implements OnInit {
    images: Image[];

    constructor(
        private imagesService: ImagesService
    ) {
    }

    ngOnInit() {
        this.images = this.imagesService.getAll();
    }

    delete(id: number): void {
        this.imagesService.delete(id);
        this.images = this.images.filter((image) => image.id !== id);
    }
}

